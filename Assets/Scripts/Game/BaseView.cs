﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Represents an abstract view for an entity
/// </summary>
public abstract class BaseView : MonoBehaviour
{
    [SerializeField]
    protected BaseEntity _entity;

    /// <summary>
    /// Set the view's entity
    /// </summary>
    public virtual void SetEntity(BaseEntity entity)
    {
        if (_entity != null)
        {
            _entity.OnEntityDestroyed.RemoveListener(DestroyView);
        }
        //if the entity is destroyed we want to kill the view too
        _entity = entity;
        _entity.OnEntityDestroyed.AddListener(DestroyView);
    }

    protected virtual void DestroyView()
    {
        Destroy(gameObject);
    }
}
