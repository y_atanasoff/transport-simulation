﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Factory used for spawning entity views
/// </summary>
public class Factory : MonoBehaviour
{
    [System.Serializable]
    public class EntityPrefab
    {
        public EntityType EntityType;
        public GameObject Prefab;
    }
    [SerializeField]
    private EntityPrefab[] _prefabs;
    [SerializeField]
    private Map _map;
    [SerializeField]
    private Transform _parent;
    private Dictionary<EntityType, GameObject> _prefabMap;

    private void Awake()
    {
        //connect to map and cache all prefabs in a dictionary
        _map.OnEntityAdded.AddListener(SpawnView);
        _prefabMap = new Dictionary<EntityType, GameObject>();
        for(int i = 0; i < _prefabs.Length; i++)
        {
            _prefabMap.Add(_prefabs[i].EntityType, _prefabs[i].Prefab);
        }
    }
    /// <summary>
    /// Spawns a game object based on the entity type
    /// </summary>
    public void SpawnView(BaseEntity entity)
    {
        GameObject prefab = null;
        if (_prefabMap.TryGetValue(entity.EntityType, out prefab))
        {
            var view = Instantiate(prefab, new Vector3(entity.Position.x, 0, entity.Position.y), Quaternion.identity).GetComponent<BaseView>();
            if (view != null)
            {
                view.SetEntity(entity);
            }
            

        }
    }

}
