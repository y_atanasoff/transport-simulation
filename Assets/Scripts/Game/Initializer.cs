﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Initializes the simulation
/// </summary>
public class Initializer : MonoBehaviour
{
    [SerializeField]
    private Map _map;
    [SerializeField]
    private RoadWorks _roads;
    [SerializeField]
    private BuildingWorks _buildings;
    //[SerializeField]
    //private BaseBuilding[] _startBuildings;


    [SerializeField]
    private BaseBuilding _producer;
    [SerializeField]
    private int _producerCount;


    [SerializeField]
    private BaseBuilding _consumer;
    [SerializeField]
    private int _consumerCount;


    [SerializeField]
    private WorldDrawer _drawer;
    [SerializeField]
    private Ticker _ticker;

    private void Start()
    {
        RebuildMap();
    }
    /// <summary>
    /// Rebuilds the map from scratch
    /// </summary>
    public void RebuildMap()
    {
        //clear everything
        _ticker.StopTick();
        _drawer.ClearMap();
        _roads.Clear();
        _buildings.Clear();

        //generate a new map
        _map.GenerateMap();

        //build a producers
        for (int i = 0; i < _producerCount; i++)
        {
            _buildings.Build(_producer, false);
        }
        //build all consumers
        for (int i = 0; i < _consumerCount; i++)
        {
            _buildings.Build(_consumer, false);
        }

        //get building positions and generate roads
        var endpoints = new Vector2Int[_buildings.Count];
        for (int i = 0; i < _buildings.Count; i++)
        {
            endpoints[i] =_buildings.GetBuilding(i).Position;
        }
        _roads.GenerateRoads(endpoints);
        //draw the map
        _drawer.DrawMap(_map);
        //start the simulation
        _ticker.StartTick();
    }
    /// <summary>
    /// Add a producer to the map on the fly
    /// </summary>
    public void AddProducer()
    {
        _buildings.Build(_producer);
        _drawer.ClearMap();
        _drawer.DrawMap(_map);
    }
    /// <summary>
    /// Add a cosumer to the map on the fly
    /// </summary>
    public void AddConsumer()
    {
        _buildings.Build(_consumer);
        _drawer.ClearMap();
        _drawer.DrawMap(_map);

    }
}
