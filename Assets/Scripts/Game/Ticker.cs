﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script that makes callbacks to all entities on the map
/// </summary>
public class Ticker : MonoBehaviour
{
    [SerializeField]
    private Map _map;

    /// <summary>
    /// Is the ticker running, are we recieving callbacks
    /// </summary>
    public bool IsRunning { get { return _isRunning; } }
    private bool _isRunning = false;

    /// <summary>
    /// Toggle the simultations
    /// </summary>
    /// <param name="run"></param>
    public void ToggleRun(bool run)
    {
        _isRunning = run;
    }
    /// <summary>
    /// Start the simulation
    /// </summary>
    public void StartTick()
    {
        _isRunning = true;
    }
    /// <summary>
    /// Stop the simulation
    /// </summary>
    public void StopTick()
    {
        _isRunning = false;
    }

    private void Update()
    {
        if (_isRunning)
        {
            for (int i = 0; i < _map.GetEntityCount(); i++)
            {
                var entity = _map.GetEntity(i);
                //if an enity has not been prepared, prepare it. Similar to Start in MonoBehaviour
                if (!entity.Prepared)
                {
                    entity.Prepare();
                }
                _map.GetEntity(i).Tick(Time.deltaTime);
            }
        }
    }
}
