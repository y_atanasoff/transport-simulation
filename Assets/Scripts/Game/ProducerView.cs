﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Represents a view for the producer building
/// </summary>
public class ProducerView : BuildingView
{
    private ProducerBuilding _producer { get { return _entity as ProducerBuilding; } }

    [SerializeField]
    private ProduceStack _stack; //visualizes the amount of product the consumer holds

    private void Start()
    {
        //set the initial product stack
        Produced(_producer.AvailableProduct);
    }

    public override void SetEntity(BaseEntity entity)
    {
        base.SetEntity(entity);
        _producer.OnConsumed.AddListener(Consumed);
        _producer.OnProduced.AddListener(Produced);
    }
    //resize the stack then the product amount has changed
    private void Produced(int amount)
    {
        _stack.Resize(amount);
    }
    //resize the stack then the product amount has changed
    private void Consumed(int amount)
    {
        _stack.Resize(-amount);
    }

    protected override void DestroyView()
    {
        if (_producer != null)
        {
            _producer.OnConsumed.RemoveListener(Consumed);
            _producer.OnConsumed.RemoveListener(Produced);
        }

        base.DestroyView();
    }



}
