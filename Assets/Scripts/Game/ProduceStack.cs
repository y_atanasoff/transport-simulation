﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Controls the gameObject that displays the amount of product
/// </summary>
public class ProduceStack : MonoBehaviour
{

    [SerializeField]
    private float _raiseAmount;

    private Vector3 _scaleCache;
    private Transform _transform;

    private void Awake()
    {
        _transform = transform;
        _scaleCache = _transform.localScale;
    }
    /// <summary>
    /// Changes the size of the product stack
    /// </summary>
    public void Resize(float amount)
    {
        _scaleCache.y = transform.localScale.y + amount * _raiseAmount;
        _transform.localScale = _scaleCache;
    }
}
