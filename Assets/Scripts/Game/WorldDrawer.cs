﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Draws the map tiles
/// </summary>
public class WorldDrawer : MonoBehaviour
{
    [System.Serializable]
    public class PrefabItem
    {
        public TerrainType Terrain;
        public GameObject Prefab;
    }
    [SerializeField]
    private PrefabItem[] _prefabs; //the prefabs used for drawing
    [SerializeField]
    private Transform _parent;//a transform under which to put all prefabs. for conviniance

    private Dictionary<TerrainType, GameObject> _prefabsDict = new Dictionary<TerrainType, GameObject>();

    private void Awake()
    {
        //cache all the prefabs in a dcitionary
        foreach (var prefab in _prefabs)
        {
            _prefabsDict.Add(prefab.Terrain, prefab.Prefab);
        }
    }
    /// <summary>
    /// Draw the entire map
    /// </summary>
    public void DrawMap(Map map)
    {
        Vector3 position = Vector3.zero;
        GameObject prefab = null;
        for (int x = 0; x < map.Width; x++)
        {
            for (int y = 0; y < map.Width; y++)
            {
                var tile = map[x, y];
                position.x = tile.Position.x;
                position.z = tile.Position.y;

                if (_prefabsDict.TryGetValue(tile.Terrain, out prefab))
                {
                    Instantiate(prefab, position, Quaternion.identity, _parent);
                }
            }
        }
    }
    /// <summary>
    /// Clears all tiles
    /// </summary>
    public void ClearMap()
    {
        foreach (Transform child in _parent)
        {
            Destroy(child.gameObject);
        }
    }
    /// <summary>
    /// Draws a single tile
    /// </summary>
    public void DrawTile(Map.MapTile tile)
    {
        GameObject prefab = null;
        if (_prefabsDict.TryGetValue(tile.Terrain, out prefab))
        {
            Vector3 position = new Vector3(tile.Position.x, 0, tile.Position.y);
            Instantiate(prefab, position, Quaternion.identity, _parent);
        }
    }


}
