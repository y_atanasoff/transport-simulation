﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Consumer view class
/// </summary>
public class ConsumerView : BuildingView
{
    private ConsumerBuilding _consumer { get { return _entity as ConsumerBuilding; } }

    [SerializeField]
    private ProduceStack _stack; //visualizes the amount of product the consumer holds

    public override void SetEntity(BaseEntity entity)
    {
        base.SetEntity(entity);
        _consumer.OnProductChanged.AddListener(Delivered);
    }

    //resize the produce stack to show the amount of product the consumer holds
    private void Delivered(int amount)
    {
        _stack.Resize(amount);
    }

    protected override void DestroyView()
    {
        if (_consumer != null)
        {
            _consumer.OnProductChanged.RemoveListener(Delivered);
        }
        base.DestroyView();
    }
}
