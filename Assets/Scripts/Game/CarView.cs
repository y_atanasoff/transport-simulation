﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents a car view
/// </summary>
public class CarView : BaseView
{
    //car entitye
    private Car _car;
    //position and forward cache
    private Vector3 _position = Vector3.zero;
    private Vector3 _forward = Vector3.zero;
    //transform cache
    private Transform _transform;

    private void Awake()
    {
        _transform = transform;
    }

    public override void SetEntity(BaseEntity entity)
    {
        base.SetEntity(entity);
        _car = _entity as Car;
        _car.OnStateChanged.AddListener(CaStateChanged);
    }
    private void CaStateChanged(CarState state)
    {
        //at the start of each state teleport the view to the start of the road
        if (state != CarState.Waiting)
        {
            _transform.position = new Vector3(_car.Position.x, 0, _car.Position.y);
            _transform.forward = new Vector3(_car.Direction.x, 0, _car.Direction.y);
        }
        //if the car is waiting, hide it
        gameObject.SetActive(state != CarState.Waiting);

    }

    private void Update()
    {
        if (_car.State != CarState.Waiting)
        {
            _position.x = _car.Position.x;
            _position.z = _car.Position.y;

            _forward.x = _car.Direction.x;
            _forward.z = _car.Direction.y;

            _transform.position = Vector3.Lerp(_transform.position, _position, 0.2f);
            _transform.forward = Vector3.Lerp(_transform.forward, _forward, 0.2f);
        }
    }
}
