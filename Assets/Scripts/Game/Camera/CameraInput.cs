﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraInput : MonoBehaviour
{
    private CameraOrbiter _orbiter;
    private Vector3 _lastMousePosition;

    private void Start()
    {
        _orbiter = GetComponent<CameraOrbiter>();
    }
    void Update()
    {
        Vector2 delta = Input.mousePosition - _lastMousePosition;
        if (Input.GetMouseButton(0))
        {
            _orbiter.Orbit(delta);
        }
        if (Input.GetMouseButton(1))
        {
            _orbiter.Pan(delta);
        }

        _orbiter.Zoom(Input.mouseScrollDelta.y);

        _lastMousePosition = Input.mousePosition;
    }
}
