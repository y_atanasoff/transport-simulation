﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraOrbiter : MonoBehaviour
{
    [SerializeField]
    private Transform _anchor;
    [SerializeField]
    private float _minZoom;
    [SerializeField]
    private float _maxZoom;

    private Vector3 _position { get { return _anchor.position; } set { _anchor.position = value; } }
    private float _distance;
    private Quaternion _rotation;
    private Camera _camera;

    private Transform _transform;

    void Start()
    {
        _transform = transform;
        _distance = Mathf.Clamp(Vector3.Distance(_transform.position, _position), _minZoom, _maxZoom);
        _camera = GetComponent<Camera>();
        _rotation = _transform.rotation;
    }

    public void Zoom(float amount)
    {
        _distance = Mathf.Clamp(_distance - amount * 0.2f, _minZoom, _maxZoom);
    }

    public void Pan(Vector2 delta)
    {
        Vector3 positionScreenSpace = _camera.WorldToScreenPoint(_position);
        positionScreenSpace -= (Vector3)delta;
        _position = _camera.ScreenToWorldPoint(positionScreenSpace);
    }

    public void Orbit(Vector2 delta)
    {
        _rotation = Quaternion.AngleAxis(-delta.y * 0.5f, _rotation * Vector3.right) * _rotation;
        _rotation = Quaternion.AngleAxis(delta.x * 0.5f, Vector3.up) * _rotation;
    }

    private void Update()
    {
        _transform.rotation = _rotation;
        _transform.position = _position + _transform.rotation * new Vector3(0, 0, -_distance);
    }
}
