﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base view for a building
/// </summary>
public class BuildingView : BaseView
{

    protected BaseBuilding _building { get { return _entity as BaseBuilding; } }

    public override void SetEntity(BaseEntity entity)
    {
        //we use entity type to match views to entities in the editor
        if (entity.EntityType != _entity.EntityType)
        {
            Debug.LogError("Trying to assing incorrect entity to view");
            DestroyView();
            return;
        }
        base.SetEntity(entity);
        transform.position = new Vector3(_building.Position.x, 0, _building.Position.y);
    }
}
