﻿using UnityEngine;

/// <summary>
/// A delegate for a road genation strategy
/// </summary>
delegate Vector2Int[][] RoadGenerator(Vector2Int[] positions, Map  map);
/// <summary>
/// Defines the terrain type
/// </summary>
public enum TerrainType { Empty = 0, Road, Building, Blocked }
/// <summary>
/// Defines how to handle disjoint regions when generating a map
/// </summary>
public enum RegionStrategy { None, KeepLargest }

/// <summary>
/// Defines how to handle road generation
/// </summary>
public enum RoadGenerationStrategy { Star, ConnectAll, ConnectAllReusingRoads }
/// <summary>
/// The state of a car
/// </summary>
public enum CarState { Waiting, Going, Returning }