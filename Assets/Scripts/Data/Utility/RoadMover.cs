﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Traverses a given road
/// </summary>
public struct RoadMover
{
    private bool _reverse; //which direction are we moving along the road
    private Vector2Int[] _path;//the list of positions that makeup the road
    private float _currentFactor;//current position from 0 to 1 along the road
    /// <summary>
    /// Position in the model world along the road
    /// </summary>
    public Vector2 CurrentPosition { get; private set; }
    /// <summary>
    /// Direction in the model world along the road
    /// </summary>
    public Vector2 CurrentDirection { get; private set; }
    /// <summary>
    /// Are there any position in the road. A incorrect road should be empty
    /// </summary>
    public bool IsEmpty {  get { return _path == null || _path.Length == 0; } }

    public RoadMover(Vector2Int[] road, bool reverse)
    {
        _reverse = reverse;
        _path = road;
        _currentFactor = 0;
        CurrentPosition = Vector2.zero;
        CurrentDirection = Vector2.zero;
    }
    /// <summary>
    /// Start the traversal 
    /// </summary>
    public void Start()
    {
        _currentFactor = _reverse ? 1 : 0;
        CurrentPosition = _reverse ? _path[_path.Length - 1] : _path[0];
        CurrentDirection = ((_reverse ? _path[_path.Length - 2] : _path[1]) - CurrentPosition).normalized;
    }
    /// <summary>
    /// Move along the road by certain delta in world units
    /// </summary>/// <param name="delta"></param>
    /// <returns>
    /// have we reached the end of the road
    /// </returns>
    public bool Move(float delta)
    {
        //convert delta from world to normalized coordinate
        float normalizedDelta = (delta / (_path.Length - 1)) * (_reverse ? -1 : 1);
        _currentFactor = Mathf.Clamp01(_currentFactor + normalizedDelta);
        //if we reached the end return false
        if (Mathf.Approximately(_currentFactor, (_reverse ? 0 : 1)))
        {
            return false;
        }

        //get the preivous and the next position based on the current factor
        float index = Mathf.Lerp(0, _path.Length - 1, _currentFactor);
        Vector2 prev = _path[Mathf.FloorToInt(index)];
        Vector2 next = _path[Mathf.CeilToInt(index)];
        //lerp the position and direction
        CurrentPosition = Vector2.Lerp(prev, next, index - Mathf.Floor(index));
        CurrentDirection = (next - prev).normalized * (_reverse ? -1 : 1);

        return true;
    }
    /// <summary>
    /// Reverse the road traversal. Go back.
    /// </summary>
    public void Reverse()
    {
        _reverse = !_reverse;
    }
}