﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Scripable Object class that defines a type of entity.
/// </summary>
[CreateAssetMenu(menuName = "EntityType")]
public class EntityType : ScriptableObject
{
}
