﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


/// <summary>
/// Represents a producer building.
/// </summary>
[CreateAssetMenu(menuName = "Buildings/Producer")]
public class ProducerBuilding : BaseBuilding
{

    [SerializeField]
    private float _productionTime;
    [SerializeField]
    private int _productionAmount = 1; 

    private int _reservedProduct;
    private int _availableProduct;
    private float _produceTimer = 0;

    /// <summary>
    /// All the product stored in the producer
    /// </summary>
    public int ProductAmount { get { return _availableProduct + _reservedProduct; } }
    /// <summary>
    /// The product available for consumers
    /// </summary>
    public int AvailableProduct { get { return _availableProduct; } }
    /// <summary>
    /// Notify when something is produced
    /// </summary>
    public ProductionChangedEvent OnProduced;
    /// <summary>
    /// Notify when something is consumed
    /// </summary>
    public ProductionChangedEvent OnConsumed;

    //used for consumer round-robin
    private LinkedList<int> _consumerIds = new LinkedList<int>();
    private LinkedListNode<int> _currentConsumerId = null;

    /// <summary>
    /// Trying to take a product from the reserved amount.
    /// </summary>
    public int TakeProduct(int amount)
    {
        if (_reservedProduct > 0)
        {
            int consumed = Mathf.Min(amount, _reservedProduct);
            _reservedProduct -= consumed;
            OnConsumed.Invoke(consumed);
            return consumed;
        }

        Debug.LogError("Error: Tried Taking Product When No Reserved");
        return 0;
    }
    /// <summary>
    /// Reserve product. The consumer can reserve product when there is available product and when it's their turn to reserve.
    /// </summary>
    /// <returns>
    /// Did we reserve any product.
    /// </returns>
    public bool ReserveProduct(int amount, int consumerId)
    {
        if (_currentConsumerId == null)
        {
            Debug.LogError("Trying to reserve a product whithout any registered consumers");
            return false;
        }
        //if it's this consumer's turn and we have available product we allow reservation
        if (consumerId == _currentConsumerId.Value && _availableProduct > 0)
        {
            int reserved = Mathf.Min(amount, _availableProduct);
            _reservedProduct += reserved;
            _availableProduct -= reserved;
            _currentConsumerId = NextConsumer(_currentConsumerId);
            return true;
        }
        return false;
    }
    /// <summary>
    /// Updates the producer.
    /// </summary>
    public override void Tick(float deltaTime)
    {
        //re
        _produceTimer -= deltaTime;
        if (_produceTimer < 0)
        {
            _availableProduct++;
            _produceTimer = _productionTime;
            OnProduced.Invoke(_productionAmount);
        }
    }
    /// <summary>
    /// Registeres the cosumer id for delivery. Unregistered consumers cannot reserve product
    /// </summary>
    public void RegisterConsumer(int consumerId)
    {
        if (_currentConsumerId != null)
        {
            _consumerIds.AddBefore(_currentConsumerId, consumerId);
            _currentConsumerId = _currentConsumerId.Previous;
        }
        else
        {
            _consumerIds.AddFirst(consumerId);
            _currentConsumerId = _consumerIds.First;
        }
    }
    /// <summary>
    /// Deregister the consumer from delivery.
    /// </summary>
    /// <param name="consumerId"></param>
    public void DeregisterConsumer(int consumerId)
    {
        if (_currentConsumerId.Value == consumerId)
        {
            _currentConsumerId = NextConsumer(_currentConsumerId);
        }
        _consumerIds.Remove(consumerId);
        if (_consumerIds.Count == 0)
        {
            _currentConsumerId = null;
        }
    }

    private LinkedListNode<int> NextConsumer(LinkedListNode<int> node)
    {
        return node.Next ?? _consumerIds.First;
    }

    [System.Serializable]
    public class ProductionChangedEvent : UnityEvent<int> { }
}
