﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Base class for all buldings, implement certain common fucntionality
/// </summary>
public class BaseBuilding : BaseEntity
{
    /// <summary>
    /// The position of the building in the model world
    /// </summary>
    public override Vector2Int Position
    {
        get
        {
            return Tile.Position;
        }
    }
    /// <summary>
    /// The tile the building is taking up
    /// </summary>
    public Map.MapTile Tile { get; private set; }
    /// <summary>
    /// Building the building on a certain tile and initliaze it 
    /// </summary>
    public virtual void Construct(Map.MapTile tile)
    {
        Tile = tile;
        Tile.Enitity = this;
        Tile.Terrain = TerrainType.Building;
        Initialize();
    }
    /// <summary>
    /// Demolishes he building.
    /// </summary>
    public override void Kill()
    {
        Tile.Enitity = null;
        base.Kill();
    }
}
