﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Consumer building
/// </summary>
[CreateAssetMenu(menuName ="Buildings/Consumer")]
public class ConsumerBuilding : BaseBuilding
{
    //global consumer id - used for assigning unique id to each consumer for registering with producers
    private static int GlobalConsumerId;

    /// <summary>
    /// The amount of product stored in the consumer
    /// </summary>
    public int ProductAmount { get; set; }
    /// <summary>
    /// Called when product is delivered
    /// </summary>
    public ProductChangedEvent OnProductChanged;


    [SerializeField]
    private int _availableCars; //number of cars we can dispatch simultaneously
    [SerializeField]
    private int _consumeAmount = 1; //the amount of product we can consume from the producer in one go
    [SerializeField]
    private BuildingWorks _buildings;
    [SerializeField]
    private RoadWorks _roads;
    [SerializeField]
    private Car _carPrototype;//car prototype for instatiating cars

    private ProducerBuilding _producer = null;//our current producer
    private int _consumerId;//our unique consumer id
    private Car[] _cars;

    /// <summary>
    /// Constuct a building
    /// </summary>
    /// <param name="tile"></param>
    public override void Construct(Map.MapTile tile)
    {
        base.Construct(tile);
        //aquire unique consumer id
        _consumerId = GlobalConsumerId++;
    }

    /// <summary>
    /// Prepare callback.
    /// </summary>
    public override void Prepare()
    {

        base.Prepare();
        //find a producer and register
        ChangeProducer();

        //spawn all needed cars
        _cars = new Car[_availableCars];
        for (int i = 0; i < _availableCars; i++)
        {
            _cars[i] = Instantiate(_carPrototype);
            _cars[i].Init();
        }
        //get notified when a building is placed, so we react if it is a producer
        _buildings.OnBuildingPlaced.AddListener(BuildingPlaced);
    }
    /// <summary>
    /// Tick callback. Check if producer has product and try to get it
    /// </summary>
    public override void Tick(float timeDelta)
    {
        //if there is no assigned producer, try to find one
        if(!_producer)
        {
            ChangeProducer();
            return;
        }

        //find any available car(that is not delivering product)
        var availableCar = Array.Find(_cars, c => c.State == CarState.Waiting);

        if (_producer.AvailableProduct > 0 && availableCar != null)
        {
            //if we can reserve product, we can dispatch a car
            if (_producer.ReserveProduct(_consumeAmount, _consumerId))
            {
                availableCar.StartCar( _roads.GetRoad(Tile.Position, _producer.Tile.Position), _producer, this, _consumeAmount);
            }
        }
    }
    /// <summary>
    /// Demolish the building and unregister from producer
    /// </summary>
    public override void Kill()
    {
        if (_producer)
        {
            _producer.OnEntityDestroyed.RemoveListener(ProducerKilled);
        }
        for (int i = 0; i < _cars.Length; i++)
        {
            _cars[i].Kill();
        }
        base.Kill();
    }

    // Find a new producer
    private void ChangeProducer()
    {
        //get a closesst producer
        var newProducer = _buildings.GetClosesBuilding<ProducerBuilding>(Tile.Position);
        if (newProducer)
        {
            //if we find a different, better producer detach and attach a new one
            if (newProducer != _producer)
            {
                DetachProducer();
                newProducer.OnEntityDestroyed.AddListener(ProducerKilled);
                newProducer.RegisterConsumer(_consumerId);
                _producer = newProducer;
            }
        }
        else
        {
            Debug.LogError("ConsumerBuilding: Could not Find Producer");
        }
    }

    // Detacha from a producer. Some cars may still be traveling to this producer when this is done. They will finish their delivery
    private void DetachProducer()
    {
        if (_producer != null)
        {
            _producer.DeregisterConsumer(_consumerId);
            _producer.OnEntityDestroyed.RemoveListener(ProducerKilled);
            _producer = null;
        }
    }
    //if a new producer is placed check if it's better
    private void BuildingPlaced(BaseBuilding building)
    {
        if (building is ProducerBuilding)
        {
            ChangeProducer();
        }
    }
    //if our producer is killed find a better one
    private void ProducerKilled()
    {
        if (_producer)
        {
            _producer.OnEntityDestroyed.RemoveListener(ProducerKilled);
            _producer = null;
            ChangeProducer();
        }
    }
    [System.Serializable]
    public class ProductChangedEvent : UnityEvent<int> { }
}
