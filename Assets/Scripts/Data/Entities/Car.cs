﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Entity Representing a Car
/// </summary>
[CreateAssetMenu( menuName = "Car")]
public class Car : BaseEntity
{
    [SerializeField]
    private float _speed;

    /// <summary>
    /// Car state changed. Called when a car stops, starts or returns.
    /// </summary>
    public StateChangedEvent OnStateChanged;

    /// <summary>
    /// Current car state
    /// </summary>
    public CarState State { get { return _carState; }  set { _carState = value; OnStateChanged.Invoke(value); } }
    /// <summary>
    /// Current car position in the model world. The car can be between tiles, so the position is non-integer
    /// </summary>
    public new Vector2 Position { get { return _mover.CurrentPosition; } }
    /// <summary>
    /// Current car direction in the model world. The car can be between tiles, so the position is non-integer
    /// </summary>
    public Vector2 Direction { get { return _mover.CurrentDirection; } }

    //object that actually moves the car along a path
    private RoadMover _mover;
    private CarState _carState;
    //the producer the car is heading towards
    ProducerBuilding _producer;
    //the producer the car has been dispatched by
    ConsumerBuilding _consumer;
    //the target amount that the car is set to aquire from the producer
    private int _targetAmount;
    //the actual amount of product the car is carying
    private int _actualAmount;

    /// <summary>
    /// Initialize the car and register it for tick callback
    /// </summary>
    public void Init()
    {
        Initialize();
    }
    /// <summary>
    /// Prepare the car
    /// </summary>
    public override void Prepare()
    {
        base.Prepare();
        State = CarState.Waiting;
    }
    /// <summary>
    /// Dispatch the car from the consumer to the producer to get a certain amount of product
    /// </summary>
    public void StartCar(RoadMover mover, ProducerBuilding producer, ConsumerBuilding consumer, int amount)
    {
        _mover = mover;
        _carState = CarState.Waiting;
        _mover.Start();
        State = CarState.Going;
        _producer = producer;
        _consumer = consumer;
        _targetAmount = amount;
    }
    /// <summary>
    /// Executed every frame
    /// </summary>
    public override void Tick(float timeDelta)
    {
        if (State == CarState.Waiting)
        {
            return;
        }
        //if we ca't go any further our jorney is finished
        bool finished = !_mover.Move(timeDelta * _speed);

        //take product and return
        if (finished && State == CarState.Going)
        {
            _actualAmount = _producer.TakeProduct(_targetAmount);
            _mover.Reverse();
            _mover.Start();
            State = CarState.Returning;
        }
        //deliver the product to the consumer
        else if (finished && State == CarState.Returning)
        {
            _consumer.ProductAmount += _actualAmount;
            _consumer.OnProductChanged.Invoke(_actualAmount);
            State = CarState.Waiting;
        }
    }

    [Serializable]
    public class StateChangedEvent : UnityEvent<CarState> { }
}
