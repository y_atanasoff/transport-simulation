﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Base class for every entity.
/// Abstract classes deriving from ScriptableObject are discouraged,
/// so we use a concrete class as our base for all entities
/// </summary>
public class BaseEntity : ScriptableObject
{
    /// <summary>
    /// Called right before an entity is destroyed
    /// </summary>
    public EntityDestroyedEvent OnEntityDestroyed;

    [SerializeField]
    protected EntityType _type;
    [SerializeField]
    protected Map _map;

    private bool _prepared = false;

    /// <summary>
    /// The type of entity
    /// </summary>
    public EntityType EntityType { get { return _type; } }
    /// <summary>
    /// The entity's current position in the model world
    /// </summary>
    public virtual Vector2Int Position { get; set; }
    /// <summary>
    /// Has the entity been prepared. Was Prepare() called?
    /// </summary>
    public bool Prepared { get { return _prepared; } }
    /// <summary>
    /// Initializes the enitity and registers it for recieving ticks.Subclassing entities have responsibility to call Initialize
    /// </summary>
    protected void Initialize()
    {
        _map.AddEntity(this);
    }
    /// <summary>
    /// Cleans up he entity and unregisters it from recieving Ticks
    /// </summary>
    protected void DestroyEntity()
    {
        OnEntityDestroyed.Invoke();
        _map.RemoveEntity(this);
        Destroy(this);
    }
    /// <summary>
    ///Prepare is called before the first tick
    /// </summary>
    public virtual void Prepare()
    {
        if (_prepared)
        {
            Debug.LogError("BaseEntity: Trying to init an entity twice" + name);
            return;
        }
        _prepared = true;
    }
    /// <summary>
    ///Called every game tick (could be every frame)
    /// </summary>
    public virtual void Tick(float timeDelta)
    {
    }
    /// <summary>
    ///Destroys and cleans up the entity;
    /// </summary>
    public virtual void Kill()
    {
        DestroyEntity();
    }

    [System.Serializable]
    public class EntityDestroyedEvent : UnityEvent { }
}
