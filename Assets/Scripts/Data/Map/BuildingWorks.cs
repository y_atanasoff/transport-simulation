﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


/// <summary>
/// Represents utility class for handling building contruction and operation
/// </summary>
[CreateAssetMenu(menuName = "Utilities/BuildingWorks")]
public class BuildingWorks : ScriptableObject
{

    public BuildingsChangedEvent OnBuildingPlaced;
    public BuildingsChangedEvent OnBuildingDemolished;

    [SerializeField]
    private Map _map;
    [SerializeField]
    private RoadWorks _roads;

    private List<BaseBuilding> _buildings = new List<BaseBuilding>();

    /// <summary>
    /// The amount of buildings currently built
    /// </summary>
    public int Count { get { return _buildings.Count; } }
    
    //cleanup all lists and callbacks 
    private void OnEnable()
    {
        _buildings = new List<BaseBuilding>();
        OnBuildingPlaced.RemoveAllListeners();
        OnBuildingDemolished.RemoveAllListeners();
    }

    /// <summary>
    /// Clear all buildings.
    /// </summary>
    public void Clear()
    {
        for (int i = 0; i < _buildings.Count; i++)
        {
            OnBuildingDemolished.Invoke(_buildings[i]);
            _buildings[i].Kill();
        }
        _buildings = new List<BaseBuilding>();
    }
    /// <summary>
    /// Get the building at the specified index
    /// </summary>
    public BaseBuilding GetBuilding(int index)
    {
        if (index < 0 || index > _buildings.Count)
        {
            Debug.LogError("BuildingWorks: Building Index Outside of Array");
            return null;
        }
        return _buildings[index];
    }
    /// <summary>
    /// Build a building at a random place and update the roads.
    /// </summary>
    public void Build(BaseBuilding building, bool updateRoads = true)
    {
        var tile = _map.GetRandomEmptyTile();
        var newBuilding = Instantiate(building);

        newBuilding.Construct(tile);
        //if need to update the roads and there are other building to connect to, then do so
        if (updateRoads && _buildings.Count > 0)
        {
            _roads.ConnectEndPoint(newBuilding.Position, _buildings.ConvertAll(b => b.Position).ToArray());
        }

        _buildings.Add(newBuilding);
        OnBuildingPlaced.Invoke(newBuilding);

    }
    /// <summary>
    /// Gets the closest building of a certain type closest to a given position. Useful for getting producers
    /// </summary>
    public T GetClosesBuilding<T>(Vector2Int position) where T : BaseBuilding
    {
        T closest = null; int distance = int.MaxValue;
        for (int i = 0; i < _buildings.Count; i++)
        {
            if (_buildings[i] is T)
            {
                int dist = _roads.GetDistanceByRoad(position, _buildings[i].Tile.Position);
                if (dist < distance)
                {
                    closest = _buildings[i] as T;
                    distance = dist;
                }
            }
        }
        return closest;
    }
    /// <summary>
    /// Demolish a building at position
    /// </summary>
    public void DemolishAt(Vector2Int position)
    {
        Demolish(_buildings.Find(b => b.Tile.Position == position));
    }

    /// <summary>
    /// Demolish a building
    /// </summary>
    public void Demolish(BaseBuilding building)
    {
        if (building != null && _buildings.Contains(building))
        {
            _buildings.Remove(building);
            OnBuildingDemolished.Invoke(building);
            building.Kill();
        }
    }

    [System.Serializable]
    public class BuildingsChangedEvent : UnityEvent<BaseBuilding> { }

}
