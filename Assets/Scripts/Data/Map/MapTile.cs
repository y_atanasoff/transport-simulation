﻿using UnityEngine;


public partial class Map : ScriptableObject
{
    /// <summary>
    /// Represents a tile in the map.
    /// </summary>
    [System.Serializable]
    public class MapTile
    {
        private Map _map;
        private TerrainType _terrainType;
        /// <summary>
        /// The type of terrain
        /// </summary>
        public TerrainType Terrain
        {
            get { return _terrainType; }
            set
            {
                _terrainType = value;
                _map.UpdateEmptyTiles(this);
            }
        }
        /// <summary>
        /// Position of the tile.
        /// </summary>
        public Vector2Int Position { get; private set; }
        /// <summary>
        /// The entity that currently occupies it.
        /// </summary>
        public BaseEntity Enitity { get; set; }
        /// <summary>
        /// Is the tile occupied.
        /// </summary>
        public bool IsOccupied
        {
            get
            {
                return (_terrainType == TerrainType.Blocked) ||
                        (_terrainType == TerrainType.Building) ||
                        (Enitity != null);
            }
        }

        public MapTile(TerrainType type, Vector2Int position, Map map)
        {
            _terrainType = type;
            Position = position;
            _map = map;
        }
    }
}