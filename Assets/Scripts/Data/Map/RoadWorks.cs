﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
/// <summary>
/// Represents a utility class for handing and creating roads.
/// Road works is used by cars to find the path to a certain endpoint(building)
/// </summary>
[CreateAssetMenu(menuName = "Utilities/RoadWorks")]
public class RoadWorks : ScriptableObject
{
    [SerializeField]
    private Map _map;
    [SerializeField]
    private float _roadTileWeight;
    [SerializeField]
    private float _emptyTileWeight;

    /// <summary>
    /// Utility class for keeping and comparing road endpoints
    /// </summary>
    private struct RoadEnds : IEquatable<RoadEnds>
    {
        public Vector2Int[] Ends;

        public RoadEnds(Vector2Int start, Vector2Int end)
        {
            Ends = new Vector2Int[] { start, end };
        }

        public bool Equals(RoadEnds other)
        {
            //two roads are the same if they have the same endpoints
            return (Ends[0] == other.Ends[0] && Ends[1] == other.Ends[1]) || (Ends[1] == other.Ends[0] && Ends[0] == other.Ends[1]);
        }

        public override int GetHashCode()
        {
            return Ends[0].GetHashCode() + Ends[1].GetHashCode();
        }
    }
    //a dictionary of possible roads
    private Dictionary<RoadEnds, Vector2Int[]> _roads = new Dictionary<RoadEnds, Vector2Int[]>();

    [SerializeField]
    private RoadGenerationStrategy _roadGenerationStrategy;
    private Dictionary<RoadGenerationStrategy, RoadGenerator> _roadStrategies;
    private RoadEnds _compareCache = new RoadEnds(Vector2Int.zero, Vector2Int.zero);
    //reset road generation strategies when the game starts
    private void OnEnable()
    {
        _roadStrategies = new Dictionary<RoadGenerationStrategy, RoadGenerator>();
        _roadStrategies.Add(RoadGenerationStrategy.Star, MapUtils.ConnectToCentroid);
        _roadStrategies.Add(RoadGenerationStrategy.ConnectAll, MapUtils.ConnectAll);
        _roadStrategies.Add(RoadGenerationStrategy.ConnectAllReusingRoads, ConnectWithRoadPreference);
    }
    /// <summary>
    /// Find the shortest path between endPoint and otherPoints, and marks it as road. Can be used for onthe fly building addition
    /// </summary>
    public void ConnectEndPoint(Vector2Int endPoint, Vector2Int[] otherPoints)
    {
        //set the pathfinding weights
        Pathfinding.RoadTileWeight = _roadTileWeight;
        Pathfinding.EmptyTileWeight = _emptyTileWeight;
        //find the shortest paths to the endpoints
        var paths = Pathfinding.FindPathsDijkstra(endPoint, otherPoints, _map);

        //iterate over all roads and mark the tiles as road
        foreach (var road in paths)
        {
            _roads.Add(new RoadEnds(road[0], road[road.Length - 1]), road);
            foreach (var position in road)
            {
                //the roads can start in the middle of buildings
                var tile = _map[position.x, position.y];
                if (tile.Terrain != TerrainType.Building)
                {
                    tile.Terrain = TerrainType.Road;
                }
            }

        }
    }
    /// <summary>
    /// Generate all roads between given endpoints
    /// </summary>
    public void GenerateRoads(Vector2Int[] endpoints)
    {
        //set the pathfinding weights
        Pathfinding.RoadTileWeight = _roadTileWeight;
        Pathfinding.EmptyTileWeight = _emptyTileWeight;
        //generate the roads based on the chosen road strategy
        var paths = _roadStrategies[_roadGenerationStrategy].Invoke(endpoints, _map);
        //iterate over all roads and mark the tiles as road
        foreach (var road in paths)
        {
            _roads.Add(new RoadEnds(road[0], road[road.Length - 1]), road);
            foreach (var position in road)
            {
                //the roads can start in the middle of buildings
                var tile = _map[position.x, position.y];
                if (tile.Terrain != TerrainType.Building)
                {
                    tile.Terrain = TerrainType.Road;
                }
            }
        }
    }
    /// <summary>
    /// Clear all the roads
    /// </summary>
    public void Clear()
    {
        _roads.Clear();
    }
    /// <summary>
    /// Get the road between two endpoints
    /// </summary>
    public RoadMover GetRoad(Vector2Int from, Vector2Int to)
    {
        bool reverse;
        return new RoadMover(GetPath(from, to, out reverse), reverse);
    }
    /// <summary>
    /// Get the distance between two points using the road network
    /// </summary>
    public int GetDistanceByRoad(Vector2Int from, Vector2Int to)
    {
        bool reverse;
        var road = GetPath(from, to, out reverse);
        return  road == null ? int.MaxValue : road.Length;
    }

    //find a path based on the given endpoints
    private Vector2Int[] GetPath(Vector2Int from, Vector2Int to, out bool reverse)
    {
        reverse = false;
        Vector2Int[] road = null;
        _compareCache.Ends[0] = from;
        _compareCache.Ends[1] = to;
        if (_roads.TryGetValue(_compareCache, out road))
        {
            reverse = (road[0] == to);
        }
        return road;
    }

    //a connection strategy that builds roads gradually, trying to reuse as much of the existing roads as possible
    private Vector2Int[][] ConnectWithRoadPreference(Vector2Int[] positions, Map map)
    {
        List<Vector2Int[]> paths = new List<Vector2Int[]>();
        var positionsList = new List<Vector2Int>(positions);

        while (positionsList.Count > 1)
        {
            //pop the first position
            var from_pos = positionsList[0];
            positionsList.RemoveAt(0);
            //connected it to all remaining position
            var generated = Pathfinding.FindPathsDijkstra(from_pos, positionsList.ToArray(), map);
            //mark the tiles as road
            foreach (var road in generated)
            {
                foreach (var position in road)
                {
                    //the roads can start in the middle of buildings
                    var tile = _map[position.x, position.y];
                    if (tile.Terrain != TerrainType.Building)
                    {
                        tile.Terrain = TerrainType.Road;
                    }
                }
            }
            paths.AddRange(generated);
        }
        return paths.ToArray();
    }
}
