﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using Random = UnityEngine.Random;
using UnityEngine.Events;
/// <summary>
/// Represents the map, holds all entities and tiles
/// </summary>
[CreateAssetMenu(menuName ="Map")]
public partial class Map : ScriptableObject
{
    //map generation attributes
    [SerializeField][Range(0, 1)]
    private float _threshold; //threshold that separates empty from blocked tiles
    [SerializeField][Range(0, 100)]
    private float _noiseScale;// the size of the noise
    [SerializeField]
    private int _seed;//random seed
    [SerializeField]
    private Vector2Int _mapSize;//size of the map
    [SerializeField]
    private RegionStrategy _adjacencyStrategy;//how do we handle disjoint regions?


    private List<BaseEntity> _enitites;//list of entities
    //todo: this might work better as a hashset
    private List<MapTile> _emptyTiles = new List<MapTile>();//cache of empty tiles
    private MapTile[,] _tiles;//the actual map

    /// <summary>
    /// Called when an entity has been added to the map
    /// </summary>
    public EntityAddedEvent OnEntityAdded;
    /// <summary>
    /// Width of the map
    /// </summary>
    public int Width { get { return _mapSize.x; } }
    /// <summary>
    /// Height of the map
    /// </summary>
    public int Height { get { return _mapSize.y; } }
    /// <summary>
    /// The tile at position x,y
    /// </summary>
    public MapTile this[int x, int y]
    {
        get
        {
            return _tiles[x, y];
        }
    }
    //clear stuff
    private void OnEnable()
    {
        hideFlags = HideFlags.DontUnloadUnusedAsset;
        _enitites = new List<BaseEntity>(100);
        _emptyTiles.Clear();
    }
    /// <summary>
    /// Returns a random tile. Useful for building and placing random stuff on the map
    /// </summary>
    public MapTile GetRandomEmptyTile()
    {
        return _emptyTiles[Random.Range(0, _emptyTiles.Count)];
    }

    /// <summary>
    /// Finds the closest empty tile. Useful if you want to place something near a certain location
    /// </summary>
    public MapTile GetClosestEmptyTile(Vector2Int position)
    {
        Vector2Int closestPoint = position;
        float distance = float.MaxValue;
        foreach (var tile in _emptyTiles)
        {
            float currentDist = Vector2.Distance(tile.Position, position);
            if (currentDist < distance)
            {
                closestPoint = tile.Position;
                distance = currentDist;
                //if the tile is exactly on our middle point we break
                if (Mathf.Approximately(distance, 0))
                {
                    break;
                }
            }
        }
        return _tiles[closestPoint.x, closestPoint.y];
    }
    /// <summary>
    /// Generates the map
    /// </summary>
    /// <param name="newSeed">If we want a new seed. if false will just generate the old map</param>
    public void GenerateMap(bool newSeed = true)
    {
        if (newSeed)
        {
            _seed = (int)(Random.value * 10000);
        }

        _enitites = new List<BaseEntity>(100);
        _emptyTiles.Clear();
        _tiles = new MapTile[_mapSize.x, _mapSize.y];

        Vector2Int position = Vector2Int.zero;

        for (int x = 0; x < _mapSize.x; x++)
        {
            for (int y = 0; y < _mapSize.x; y++)
            {
                position.x = x; position.y = y;
                //sample the perlin noise function for each tile of the map
                float value = Mathf.PerlinNoise(_seed + (x / _noiseScale), _seed + (y / _noiseScale));
                //decide on the terain type based on the threshold
                TerrainType tileType = value > _threshold ? TerrainType.Blocked : TerrainType.Empty;
                _tiles[x, y] = new MapTile(tileType, position, this);
                //cache the empty tiles
                if (tileType == TerrainType.Empty)
                {
                    _emptyTiles.Add(_tiles[x, y]);
                }
            }
        }

        //if don't want to handle disjoint regions just return
        if (_adjacencyStrategy == RegionStrategy.None)
        {
            return;
        }

        var passableMapCache = new bool[_mapSize.x, _mapSize.y];
        var largestArea = new HashSet<MapTile>();
        var blockedArea = new HashSet<MapTile>();

        HashSet<MapTile> emptyTiles = new HashSet<MapTile>();

        //cach the empty tiles and mark the tiles as passable in the map
        foreach (var tile in _tiles)
        {
            if (tile.Terrain == TerrainType.Empty)
            {
                emptyTiles.Add(tile);
                passableMapCache[tile.Position.x, tile.Position.y] = true;
            }
            else
            {
                passableMapCache[tile.Position.x, tile.Position.y] = false;
            }
        }

        //we get a random tile and grow the are around it 
        //to find the largest connected clique - this will be our new available area
        while (emptyTiles.Count > 0)
        {

            //get a tile
            var randomTile = emptyTiles.First();
            //find the surrnounding connected area
            var area = MapUtils.GrowArea(randomTile, _tiles, ref passableMapCache);
            //remove that are from the cache
            emptyTiles.ExceptWith(area);
            //if it is the larges are use it as such
            if (area.Count > largestArea.Count)
            {
                blockedArea.UnionWith(largestArea);
                largestArea = area;
            }
            else
            {
                blockedArea.UnionWith(area);
            }
        }

        if (_adjacencyStrategy == RegionStrategy.KeepLargest)
        {
            //mark all smaller disjoint areas a blocked from now on
            foreach (var tile in blockedArea)
            {
                _tiles[tile.Position.x, tile.Position.y].Terrain = TerrainType.Blocked;
            }
        }
    }
    //utility method for keeping the empty tile cache up to date
    private void UpdateEmptyTiles(MapTile tile)
    {
        if (tile.Terrain == TerrainType.Empty)
        {
            _emptyTiles.Add(tile);
        }
        else
        {
            _emptyTiles.Remove(tile);
        }
    }
    /// <summary>
    /// returns the amount of entities
    /// </summary>
    public int GetEntityCount()
    {
        return _enitites.Count;
    }
    /// <summary>
    /// get an entity at a certain index
    /// </summary>
    public BaseEntity GetEntity(int index)
    {
        if (index < 0 || index > _enitites.Count)
        {
            Debug.LogError("Map: Entity Index Outside of Array");
            return null;
        }
        return _enitites[index];
    }
    /// <summary>
    /// Register an entity with the map
    /// </summary>
    public void AddEntity(BaseEntity entity)
    {
        _enitites.Add(entity);
        OnEntityAdded.Invoke(entity);
    }
    /// <summary>
    /// Remove an entity from the map
    /// </summary>
    public void RemoveEntity(BaseEntity entity)
    {
        if(!_enitites.Contains(entity))
        {
            Debug.LogError("Map: Trying To Remove Entities That Are Not Added " + entity.name);
        }
        _enitites.Remove(entity);
    }

    [Serializable]
    public class EntityAddedEvent : UnityEvent<BaseEntity> { }
}
