﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contains algorithms for pathfinding
/// </summary>
public static class  Pathfinding
{
    /// <summary>
    /// utility class for finding the shortest path
    /// </summary>
    private class TraversalData
    {
        public float Distance;
        public Vector2Int Pos;
        public Vector2Int Parent;
        public bool Visited;
        public bool Inserted;
    }
    /// <summary>
    /// Weight for road tiles
    /// </summary>
    public static float RoadTileWeight = 1;
    /// <summary>
    /// Weight for empty tiles
    /// </summary>
    public static float EmptyTileWeight = 1;
    /// <summary>
    /// Implements basic dijkstra from on to multiple points
    /// </summary>
    public static Vector2Int[][] FindPathsDijkstra(Vector2Int from, Vector2Int[] to, Map map)
    {
        //cache the destinations and size
        Vector2Int size = new Vector2Int(map.Width, map.Height);
        List<Vector2Int> destinations = new List<Vector2Int>(to);
        var distances = new TraversalData[size.x, size.y];
        //init the traversal data
        for (int x = 0; x < size.x; x++)
        {
            for (int y = 0; y < size.y; y++)
            {
                distances[x, y] = new TraversalData();
                distances[x, y].Distance = float.MaxValue;
                distances[x, y].Visited = false;
                distances[x, y].Inserted = false;
                distances[x, y].Parent = new Vector2Int(-1, -1);
                distances[x, y].Pos = new Vector2Int(x, y);
            }
        }
        //initialize the fringe and push the starting position
        distances[from.x, from.y].Distance = 0;
        List<TraversalData> fringe = new List<TraversalData>();
        fringe.Add(distances[from.x, from.y]);
        //while we still can traverse something
        while (fringe.Count > 0)
        {
            //find the tile with the shortest distance to the start the fringe
            TraversalData currentTile = fringe[0];
            int minIndex = 0;
            for (int i = 0; i < fringe.Count; i++)
            {
                if (currentTile.Distance > fringe[i].Distance)
                {
                    currentTile = fringe[i];
                    minIndex = i;
                }
            }
            fringe.RemoveAt(minIndex);
            //if we have visited this tile, skip
            if (currentTile.Visited)
            {
                continue;
            }

            float tileDistance = currentTile.Distance;
            //traverse all of the neighbours
            foreach (var offset in MapUtils.Offsets)
            {
                var pos = offset + currentTile.Pos;
                //only choose empty Tiles
                if (pos.x >= 0 && pos.y >= 0 && pos.x < size.x && pos.y < size.y &&
                    (!  map[pos.x, pos.y].IsOccupied || destinations.Contains(pos)))
                {
                    float distance = tileDistance + 
                        (map[pos.x, pos.y].Terrain == TerrainType.Road ? RoadTileWeight : EmptyTileWeight);
                    var data = distances[pos.x, pos.y];
                    //if we haven't visited the tile add it to the fringe
                    if (!data.Visited && distance < data.Distance)
                    {
                        data.Distance = distance;
                        data.Parent = currentTile.Pos;
                        if (!data.Inserted)
                        {
                            fringe.Add(data);
                            data.Inserted = true;
                        }
                    }
                }
            }

            distances[currentTile.Pos.x, currentTile.Pos.y].Visited = true;
            //if we have visited our destination, pop the position from our destintation cache
            if (destinations.Contains(currentTile.Pos))
            {
                destinations.Remove(currentTile.Pos);
                //if we found the path to all our destinatios then the job is done
                if (destinations.Count == 0)
                {
                    break;
                }
            }
        }
        //reconstuct the path based on the traversal data
        Vector2Int[][] paths = new Vector2Int[to.Length][];
        for (int i = 0; i < to.Length; i++)
        {
            List<Vector2Int> path = new List<Vector2Int>(size.x);
            Vector2Int current = to[i];
            while (current.x >= 0 && current.y >= 0)
            {
                path.Add(current);
                current = distances[current.x, current.y].Parent;
            }
            paths[i] = path.ToArray();
            //so far the paths was the other way around
            Array.Reverse(paths[i]);
        }
        return paths;
    }
}
