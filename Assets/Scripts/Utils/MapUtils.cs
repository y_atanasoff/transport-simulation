﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


/// <summary>
/// Utility methos for map and road generation
/// </summary>
public static class MapUtils
{
    /// <summary>
    /// Cached offsets in the map - up, down, left, right
    /// </summary>
    public static Vector2Int[] Offsets = { Vector2Int.up, Vector2Int.down, Vector2Int.left, Vector2Int.right };

    /// <summary>
    /// Finds the largest connected area around the tile and update the traversal cache. 
    /// The traversal keeps all the traversed tiles so far
    /// </summary>

    public static HashSet<Map.MapTile> GrowArea(Map.MapTile centerTile, Map.MapTile[,] map, ref bool[,] traversalCache)
    {
        //initialized the final area and the fringe
        HashSet<Map.MapTile> area = new HashSet<Map.MapTile>();
        Queue<Map.MapTile> fringe = new Queue<Map.MapTile>();
        
        //get the size of the map
        int width = map.GetLength(0);
        int height = map.GetLength(1);

        //push the first tile in the fringe
        fringe.Enqueue(centerTile);

        while (fringe.Count > 0)
        {
            //get a tile from the fringe
            var tile = fringe.Dequeue();

            foreach (var offset in Offsets)
            {
                //if the tile is inside the map and has not been visited, push it to the fringe
                var pos = tile.Position + offset;
                if (pos.x >= 0 && pos.y >= 0 && pos.x < width && pos.y < height)
                {
                    if (traversalCache[pos.x, pos.y])
                    {
                        fringe.Enqueue(map[pos.x, pos.y]);
                        traversalCache[pos.x, pos.y] = false;
                    }
                }
            }
            //add the tile to the area
            area.Add(tile);
        }
        return area;
    }
    /// <summary>
    /// Road generation strategy that connects all edpoints by their shortest repsective paths
    /// </summary>
    public static Vector2Int[][] ConnectAll(Vector2Int[] positions, Map map)
    {
        List<Vector2Int[]> paths = new List<Vector2Int[]>();
        var positionsList = new List<Vector2Int>(positions);

        while (positionsList.Count > 1)
        {
            //pop the first position
            var from_pos = positionsList[0];
            positionsList.RemoveAt(0);
            //push all the shortest paths to that point
            paths.AddRange(Pathfinding.FindPathsDijkstra(from_pos, positionsList.ToArray(), map));
        }
        return paths.ToArray();
    }
    /// <summary>
    /// A road strategy that finds the centroid of all positions and connects all points to that centroid
    /// </summary>
    public static Vector2Int[][] ConnectToCentroid(Vector2Int[] positions, Map map)
    {
        //compute a point that is between all buildings
        Vector2 centroid = (Vector2)positions.Aggregate((sum, element) => sum + element) / positions.Length;
        Vector2Int centroidTile = map.GetClosestEmptyTile(new Vector2Int((int)centroid.x, (int)centroid.y)).Position;

        List<Vector2Int[]> paths = new List<Vector2Int[]>();

        //compute the shortest paths to all points from the middle point
        var centroidPaths = Pathfinding.FindPathsDijkstra(centroidTile, positions, map);

        //so far all the paths are from the centroid to the position
        //we need to meld those
        for (int i = 0; i < centroidPaths.Length - 1; i++)
        {
            //get the a path and reverse if - make if from the position to the centroid
            var halfpath = centroidPaths[i];
            Array.Reverse(halfpath);
            for (int j = i + 1; j < centroidPaths.Length; j++)
            {
                //we will attach this path to the halfpath
                var remainder = centroidPaths[j];

                //find the first / last point of difference 
                //sometimes the path overlap, so we want to exclude this region
                int p = 0;
                for (; p < Mathf.Min(halfpath.Length, remainder.Length); p++)
                {
                    if (halfpath[halfpath.Length - 1 - p] != remainder[p])
                    {
                        break;
                    }
                }
                //create the final path
                var fullPath = new Vector2Int[halfpath.Length + remainder.Length - 2 * p + 1];
                //copy the data from both halfs
                Array.Copy(halfpath, fullPath, halfpath.Length - p + 1);
                Array.Copy(remainder, p, fullPath, halfpath.Length - p + 1, remainder.Length - p);

                paths.Add(fullPath);
            }
        }
        return paths.ToArray();
    }


}
